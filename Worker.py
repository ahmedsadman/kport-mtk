from subprocess import call
import os, shutil

defaultDir = os.getcwd() # get current working directory
def CleanUp():
	os.chdir(defaultDir)
	if os.path.exists("stock"):
		shutil.rmtree("stock")
		os.mkdir("stock")
	else: os.mkdir("stock")
	if os.path.exists("port"):
		shutil.rmtree("port")
		os.mkdir("port")
	else: os.mkdir("port")
	if os.path.exists("output"):
		shutil.rmtree("output")

def Validate(field1, field2, mode="strict"):
	if mode == "strict":
		if field1 == "" or field2 == "": 
			return 1
		elif field1[-3:] != "img" or field2[-3:] != "img":
			return 2
		else:
			return 0
	else:
		realField = field1 or field2 # returns the filed which has a value (not blank)
		if field1 == "" and field2 == "":
			return 1
		elif realField[-3:] != "img":
			return 2
		else:
			return 0

def Unpack(stock_file, port_file):
	CleanUp()
	shutil.copy2("bootimg.exe", "stock") # file to run the core commands
	shutil.copy2("bootimg.exe", "port") # file to run the core commands 
	if bool(stock_file): shutil.copy2(stock_file, "stock") # the stock boot file
	if bool(port_file): shutil.copy2(port_file, "port") # the port boot file

	os.chdir("stock")
	if os.path.exists("boot.img"): call("bootimg.exe --unpack-bootimg boot.img") # run the program and wait
	os.chdir(defaultDir)
	os.chdir("port")
	if os.path.exists("boot.img"): call("bootimg.exe --unpack-bootimg boot.img") # run the program and wait
	print "Success"
	os.chdir(defaultDir)

def Repack():
	try:
		os.chdir("stock")
		call("bootimg.exe --repack-bootimg")
	except: pass
	os.chdir(defaultDir)
	try:
		os.chdir("port")
		call("bootimg.exe --repack-bootimg")
	except: pass
	os.chdir(defaultDir)

def PortKernel(stock_file, port_file, statusBar):
	statusBar.SetStatusText("Please Wait...")
	Unpack(stock_file, port_file)
	os.remove(r'port\kernel')
	shutil.copy2(r'stock\kernel', r'port\kernel')
	os.chdir("port")
	call("bootimg.exe --repack-bootimg")
	os.chdir(defaultDir)
	os.mkdir("output")
	shutil.move(r'port\boot-new.img', os.path.join(defaultDir, "output"))
	statusBar.SetStatusText("Done. New image is in 'output' folder")

def main():
	pass

if __name__ == "__main__":
	main()
