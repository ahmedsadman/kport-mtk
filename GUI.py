import wx, Worker
from subprocess import call
from wx.lib.wordwrap import wordwrap

__metaclass__ = type
class FileDrop(wx.FileDropTarget):
	def __init__(self, window, field):
		super(FileDrop, self).__init__()
		self.window = window
		self.field = field
	
	def OnDropFiles(self, x, y, filenames):
		self.filepath = filenames[0]
		self.field.SetValue(self.filepath)

class GUI(wx.Frame):
	def __init__(self, *args, **kwargs):
		super(GUI, self).__init__(*args, **kwargs)
		self.InitUI()

	def InitUI(self):
		panel = wx.Panel(self)
		panel.SetBackgroundColour("WHITE")
		hbox_radio = wx.BoxSizer(wx.HORIZONTAL)
		vbox = wx.BoxSizer(wx.VERTICAL)
		hbox_buttons = wx.BoxSizer(wx.HORIZONTAL)
		font = wx.Font(pointSize = 12, family = wx.DEFAULT, faceName = "Candara", style = wx.NORMAL, weight = wx.LIGHT)
		font_header = wx.Font(pointSize = 16, family = wx.DEFAULT, faceName = "Verdana", style = wx.NORMAL, weight = wx.NORMAL)
		font_textfield = wx.Font(pointSize = 11, family = wx.DEFAULT, faceName = "Candara", style = wx.NORMAL, weight = wx.LIGHT)
		
		# Menubar
		menubar = wx.MenuBar()
		filemenu = wx.Menu()
		
		about_menu = filemenu.Append(wx.ID_ANY, 'About')
		quit_menu = filemenu.Append(wx.ID_EXIT, 'Quit', 'Quit Application')
		
		menubar.Append(filemenu, '&Menu')
		self.SetMenuBar(menubar)
		# --- #

		self.rb_pack = wx.RadioButton(panel, label='Unpack/Repack', style=wx.RB_GROUP, id=wx.ID_ANY)
		self.rb_port = wx.RadioButton(panel, label='Port Kernel', id=wx.ID_ANY)
		self.rb_port.SetValue(True)
		self.st_header = wx.StaticText(panel, label = 'KPort v0.1')
		self.st_stockfile = wx.StaticText(panel, label = 'Stock Image (Drag && Drop)')
		self.st_packfile = wx.StaticText(panel, label = 'Port Image (Drag && Drop)')
		self.st_stockfile.SetFont(font)
		self.st_packfile.SetFont(font)
		self.rb_port.SetFont(font)
		self.rb_pack.SetFont(font)
		self.st_header.SetFont(font_header)
		self.st_header.SetForegroundColour("Blue")

		self.rb_port.Bind(wx.EVT_RADIOBUTTON, self.OnRadioChange)
		self.rb_pack.Bind(wx.EVT_RADIOBUTTON, self.OnRadioChange)
		self.Bind(wx.EVT_MENU, self.About, about_menu)
		self.Bind(wx.EVT_MENU, self.OnQuit, quit_menu)

		self.txt_stock = wx.TextCtrl(panel, size=((280, -1)))
		self.txt_port = wx.TextCtrl(panel, size=((280, -1)))
		self.txt_stock.SetFont(font_textfield)
		self.txt_port.SetFont(font_textfield)
		self.txt_stock.SetForegroundColour("Grey")
		self.txt_port.SetForegroundColour("Grey")

		self.bt_unpack = wx.Button(panel, label = 'Unpack', size = ((88, 30)))
		self.bt_repack = wx.Button(panel, label = 'Repack', size = ((88, 30)))
		self.bt_port = wx.Button(panel, label = 'Port Kernel', size = ((90, 30)))
		self.bt_repack.SetFont(font)
		self.bt_unpack.SetFont(font)
		self.bt_port.SetFont(font)
		self.bt_repack.Disable()
		self.bt_unpack.Disable()

		self.bt_port.SetToolTipString("Directly port the kernel (by replacing \"kernel\" file)")
		self.bt_unpack.SetToolTipString("Unpack boot image of port and stock")
		self.bt_repack.SetToolTipString("Repack boot image of the port folder")

		hbox_radio.Add(self.rb_pack)
		hbox_radio.Add(self.rb_port, flag = wx.LEFT, border = 10)
		vbox.Add((-1, 4))
		vbox.Add(self.st_header, flag = wx.ALIGN_CENTRE | wx.TOP, border = 5)
		vbox.Add(hbox_radio, flag = wx.ALL, border = 10)
		vbox.Add((-1, 8))
		vbox.Add(self.st_stockfile, flag = wx.LEFT, border = 15)
		vbox.Add(self.txt_stock, flag = wx.LEFT | wx.RIGHT, border = 15)
		vbox.Add((-1, 10))
		vbox.Add(self.st_packfile, flag = wx.LEFT, border = 15)
		vbox.Add(self.txt_port, flag = wx.LEFT | wx.RIGHT, border = 15)
		vbox.Add((-1, 5))

		# Adding the buttons Horizontally
		hbox_buttons.Add(self.bt_unpack, flag = wx.LEFT, border = 13)
		hbox_buttons.Add(self.bt_repack, flag = wx.LEFT, border = 10)
		hbox_buttons.Add(self.bt_port, flag = wx.LEFT, border = 10)
		vbox.Add((-1, 10))
		vbox.Add(hbox_buttons)
		vbox.Add((-1, 26))
		panel.SetSizer(vbox)

		drop1 = FileDrop(self.txt_stock, self.txt_stock)
		drop2 = FileDrop(self.txt_port, self.txt_port)
		self.txt_stock.SetDropTarget(drop1)
		self.txt_port.SetDropTarget(drop2)
		
		# sets the statusbar
		self.sb = self.CreateStatusBar()
		self.sb.SetStatusText("Ready")
		self.sb.SetFont(font_textfield)

		# button events
		self.bt_port.Bind(wx.EVT_BUTTON, self.OnPort)
		self.bt_unpack.Bind(wx.EVT_BUTTON, self.OnUnpack)
		self.bt_repack.Bind(wx.EVT_BUTTON, self.OnRepack)

		# App Settings
		vbox.Fit(self)
		self.SetTitle("KPORT 0.1")
		self.Centre()
		self.Show(True)

	def OnRadioChange(self, e):
		port = self.rb_port.GetId()
		pack = self.rb_pack.GetId()
		selected = e.GetEventObject()
		if selected.GetId() == pack:
			self.bt_port.Disable()
			self.bt_unpack.Enable()
			self.bt_repack.Enable()
		else:
			self.bt_port.Enable()
			self.bt_repack.Disable()
			self.bt_unpack.Disable()

	def OnPort(self, e):
		stock_file = self.txt_stock.GetValue()
		port_file = self.txt_port.GetValue()
		errorCheck = Worker.Validate(stock_file, port_file)

		if errorCheck == 1:
			msg = "No input file given" # 1 = blank input field
		elif errorCheck == 2:
			msg = "Invalid file given. File extension must be \'.img'" # 2 = invalid extension
		else: 
			msg = None
		if bool(msg):
			wx.MessageBox(msg, 'Error', wx.OK | wx.ICON_ERROR)
			return # stop the function from continuing
		Worker.PortKernel(stock_file, port_file, self.sb)

	def OnUnpack(self, e):
		stock_file = self.txt_stock.GetValue()
		port_file = self.txt_port.GetValue()
		if Worker.Validate(stock_file, port_file, "normal") == 0:
			self.sb.SetStatusText("Please wait...")
			Worker.Unpack(stock_file, port_file)
			self.sb.SetStatusText("Unpacking complete")
		else: 
			wx.MessageBox("Invalid Input", 'Error', wx.OK | wx.ICON_ERROR)

	def OnRepack(self, e):
		self.sb.SetStatusText("Please wait...")
		Worker.Repack()
		self.sb.SetStatusText("Repacking complete")

	def About(self, e):
		aboutMsg = '''
		Easily unpack-repack boot.img files and port Kernels in a single click. Please note that it's tested only on Mediatek based devices.

		License: If you think that you can improve my work, you are welcome, as long as you give proper credits. If you want to make small changes, give a push request in bitbucket.
		This program is based on the command line program to process files, credit goes to the original developer for the tool.
		
		Developer: Sadman Muhib Samyo
		E-Mail: ahmedsadman.211@gmail.com
		'''
		wx.MessageBox(wordwrap(aboutMsg, 300, wx.ClientDC(self)), 'About', wx.OK | wx.ICON_INFORMATION)

	def OnQuit(self, e):
		self.Close()

def main():
	gui = wx.App()
	GUI(None, style=wx.STAY_ON_TOP | wx.CAPTION | wx.CLOSE_BOX | wx.MINIMIZE_BOX)
	gui.MainLoop()

if __name__ == "__main__":
	main()
